# Lógica de programação Virado no Jiraya - DevDojo


## 1 Lógica de programação Virado no Jiraya - Introdução ao curso

## 2 Lógica Virado no Jiraya 01 - Filtrando os fracos

## 3 Lógica Virado no Jiraya 02 - Algoritmos e linguagens de programação

## 4 Lógica Virado no Jiraya 03 - Como Java Funciona

## 5 Lógica Virado no Jiraya 04 - Instalando Java

## 6 Lógica Virado no Jiraya 05 - Meu primeiro programa

## 7 Lógica Virado no Jiraya 06 - Memória volátil

## 8 Lógica Virado no Jiraya 07 - Variáveis e tipos primitivos

## 9 Lógica Virado no Jiraya 08 - Comentários + Exercícios

## 10 Lógica Virado no Jiraya 09 - Instalando IDE IntelliJ Idea

## 11 Lógica Virado no Jiraya 10 - Valores literais + exercícios

## 12 Lógica Virado no Jiraya 11 - Reutilizando variáveis + exercícios

## 13 Lógica Virado no Jiraya 12 - String, char e boolean

## 14 Lógica Virado no Jiraya 13 - Controle de fluxo if-else pt 01

## 15 Lógica Virado no Jiraya 14 - Controle de fluxo if-else pt 02

## 16 Lógica Virado no Jiraya 15 - Controle de fluxo if-else pt 03

## 17 Lógica Virado no Jiraya 16 - Controle de fluxo IF - ELSE pt 04

## 18 Lógica Virado no Jiraya 17 - Controle de fluxo IF - ELSE pt 05

## 19 Lógica Virado no Jiraya 18 - Controle de fluxo IF - ELSE pt 06

## 20 Lógica Virado no Jiraya 19 - Lendo dados do teclado

## 21 Lógica Virado no Jiraya 20 - Controle de fluxo IF - ELSE pt 07

## 22 Lógica Virado no Jiraya 21 - Controle de fluxo IF - ELSE pt 08

## 23 Lógica Virado no Jiraya 22 - Controle de fluxo IF - ELSE pt 09

## 24 Lógica Virado no Jiraya 23 - Controle de fluxo IF - ELSE pt 10

## 25 Lógica Virado no Jiraya 24 - Controle de fluxo IF - ELSE pt 11

## 26 Lógica Virado no Jiraya 25 - Estruturando o código em pacotes

## 27 Lógica Virado no Jiraya 26 - Switch case pt 01

## 28 Lógica Virado no Jiraya 27 - Switch case pt 02.mp4

## 29 Lógica Virado no Jiraya 28 - While pt 01

## 30 Lógica Virado no Jiraya 29 - While pt 02

## 31 Lógica Virado no Jiraya 30 - While pt 03

## 32 Lógica Virado no Jiraya 31 - While pt 04

## 33 Lógica Virado no Jiraya 32 - Do-While pt 05

## 34 Lógica Virado no Jiraya 33 - For pt 01

## 35 Lógica Virado no Jiraya 34 - For pt 02

## 36 Lógica Virado no Jiraya 35 - For pt 03

## 37 Lógica Virado no Jiraya 36 - Arrays pt 01

## 38 Lógica Virado no Jiraya 37 - Arrays pt 02

## 39 Lógica Virado no Jiraya 38 - Arrays pt 03

## 40 Lógica Virado no Jiraya 39 - Arrays pt 04

## 41 Lógica Virado no Jiraya 40 - Arrays Multidimensionais pt 01

## 42 Lógica Virado no Jiraya 41 - Arrays Multidimensionais pt 02

## 43 Lógica Virado no Jiraya 42 - Arrays Multidimensionais pt 03

## 44 Lógica Virado no Jiraya 43 - Acabou! E agora?
