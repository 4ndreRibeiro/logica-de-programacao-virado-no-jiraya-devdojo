package devDojo.condicionais.ifelse;

import java.util.Scanner;

/**
 * @author André Ribeiro
 *
 */
/*
 * Receba um numero inteiro do usuário e imprima o dia da semana correspondente
 * sendo 1 segunda-feira
 */
public class DiaDaSemana {
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		System.out.println("Entra com um numero de 1 a 7");
		int dia = input.nextInt();
		
		if (dia == 1) {
			System.out.println("Segunda-feira");
		} else if (dia == 2) {
			System.out.println("Terça-feira");
		} else if (dia == 3) {
			System.out.println("Quarta-feira");
		} else if (dia == 4) {
			System.out.println("Quinta-feira");
		} else if (dia == 5) {
			System.out.println("Sexta-feira");
		} else if (dia == 6) {
			System.out.println("Sábado");
		} else if (dia == 7) {
			System.out.println("Domingo");
		} else {
			System.out.println("Dia invalido");
		}
	}
}