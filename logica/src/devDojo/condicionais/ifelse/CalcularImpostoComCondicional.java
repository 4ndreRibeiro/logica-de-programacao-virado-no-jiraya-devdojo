package devDojo.condicionais.ifelse;
/**
 * @author André Ribeiro
 *
 */
/*
 * Dado um determinado salário se o salário for maior que 4500 imprima 30% do valor senão imprima 15% do valor.
 * Desafio: Utilize apenas uma variavel para guardar o resultado e imprimir no final
 * Desafio2: Diga na impressão de é 30% ou 10$
 */
public class CalcularImpostoComCondicional {
	public static void main(String[] args) {
		float salario = 4700.50F;
		float resultado = 0F;
		String porcentagem = "";
		if (salario > 4500) {
			resultado = salario * 0.3F;
			porcentagem = "30%";
		}else {
			resultado = salario * 0.1F;
			porcentagem = "10%";
		}
		System.out.println("O valor final em porcentagem de " + porcentagem + " é de " + resultado);
	}
}