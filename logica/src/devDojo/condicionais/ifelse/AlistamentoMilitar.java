package devDojo.condicionais.ifelse;

/**
*
* @author André Ribeiro
*/
public class AlistamentoMilitar {

	public static void main(String[] args) {
		char sexo = 'F';
		int idade = 15;

		if ((sexo == 'M' || sexo == 'F') && idade < 18) {
			System.out.println("Alistamento não permitido");
		}else if (sexo == 'M' && idade >= 18) {
			System.out.println("alistamento obrigatorio");
		}else if (sexo == 'F' && idade >= 18) {
			System.out.println("deseja se alistar");
		}
	}
}