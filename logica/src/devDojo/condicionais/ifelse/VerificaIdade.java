package devDojo.condicionais.ifelse;

/**
 * @author André Ribeiro
 *
 */
/*
 * Receba uma idade como entrada
 * Se a idade for maior que 18 imprima "Adulto"
 * Senão imprima "Ainda não é adulto
 * */

public class VerificaIdade {
	public static void main(String[] args) {
		int idade = 15;
		
		if (idade > 18) {
			System.out.println("Adulto");
			
		}
		System.out.println("Ainda não e Adulto");

	}
}