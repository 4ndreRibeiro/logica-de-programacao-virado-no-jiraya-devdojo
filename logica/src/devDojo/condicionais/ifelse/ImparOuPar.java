package devDojo.condicionais.ifelse;

/**
 * @author André Ribeiro
 *
 */
public class ImparOuPar {
	public static void main(String[] args) {
		int numero = 121212111;
		if ((numero % 2) == 0) {
			System.out.println("Ele é PAR " + numero);
		}else {
			System.out.println("Ele é IMPAR " + numero);
		}
	}
}