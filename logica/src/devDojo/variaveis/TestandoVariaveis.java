package devDojo.variaveis;

/**
 * @author André Ribeiro
 *
 */
public class TestandoVariaveis {

	public static void main(String[] args) {
		char masculino = 'M';
		char feminino = 'F';
		boolean condicao = false;
		System.out.println(masculino);
		System.out.println(feminino);
		System.out.println(condicao);

	}
}