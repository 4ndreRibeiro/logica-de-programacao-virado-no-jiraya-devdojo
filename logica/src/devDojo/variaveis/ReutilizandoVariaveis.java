package devDojo.variaveis;

/**
 * @author André Ribeiro
 *
 */
/*
 * Crie uma variável que ira guardar o valor de umsalario
 * Calcule a porcentagem desse salário, sendo os valores da pocentagem:
 * 30%
 * 15%
 * 5%
 * A cada vez que você calcular guarde o resultado em uma variavel
 * Imprima o resultado e reutilize a variavel que guarda o esultado para o novo calculo
 * 
 * */
public class ReutilizandoVariaveis {
	public static void main(String[] args) {
		double salario = 5000;
		double resultado = salario * 0.3;
		System.out.println("30% eh: " + resultado);
		resultado = salario * 0.15;
		System.out.println("15% eh: " + resultado);
		resultado = salario * 0.05;
		System.out.println("5% eh: " + resultado);
		
	}
}