package devDojo.variaveis;

/**
 * @author André Ribeiro
 *
 */
/*
 * Crie uma classe que calcule uma determinada porcentagem de um dado salário
 * Crie uma classe
 * Aceitar um valor de entrada para salário
 * Definir o valor da porcentagem
 * Calcular a porcentagem
 */
public class CalculadorImposto {
        public static void main(String[] args) {
        float salario = 2500.00F;
        float porcentagem = 30;
        float porcentagemDoSalario = salario * porcentagem / 100;
        System.out.println(porcentagemDoSalario);

        }
}