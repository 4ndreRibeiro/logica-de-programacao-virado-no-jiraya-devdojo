/*
Cria uma clasee chamada CalculadorDiferencaIdade
Crie uma variável para guardar a primeira idade
Crie uma variável para guardar a segunda idade
Crie uma variável para guardar a diferenca entre a idade 1 e a idade 2
Imprima a diferença no console
*/
public class CalculadorDiferencaIdade {
	public static void main(String[] args) {
		int primeiraIdade = 36;
		int segundaIdade = 28;
		int diferenca = (primeiraIdade - segundaIdade);
		System.out.println("A diferença de idade eh: " + diferenca);
	}
}